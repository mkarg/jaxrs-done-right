package eu.headcrashing.jaxrsdoneright.xi;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class BakeryApplication extends Application {

    public Set<Class<?>> getClasses() {
        return new HashSet<>(Arrays.asList(BakeryResource.class, EatableMessageBodyReaderV1.class,
                EatableMessageBodyReaderV2.class, EatableMessageBodyWriterV1.class, EatableMessageBodyWriterV2.class));
    }

}