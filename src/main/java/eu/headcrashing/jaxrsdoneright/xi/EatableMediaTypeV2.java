package eu.headcrashing.jaxrsdoneright.xi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

class EatableMediaTypeV2 {

    static void render(Eatable eatable, OutputStream outputStream) throws IOException {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(outputStream))) {
            writer.write(render(eatable));
        }
    }

    static String render(Eatable eatable) {
        if (eatable instanceof Apple)
            return "[v2:APPLE]";
        if (eatable instanceof Pie)
            return "[v2:PIE]";
        throw new RuntimeException("Cannot render only Apple and Pie.");
    }

    static Eatable parse(InputStream inputStream) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            return parse(reader.readLine());
        }
    }

    static Eatable parse(String entity) {
        if (entity.equals("[v2:APPLE]"))
            return new Apple();
        if (entity.equals("[v2:PIE]"))
            return new Pie();
        throw new RuntimeException("Cannot parse only Apple and Pie.");
    }

}
