package eu.headcrashing.jaxrsdoneright.xi;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Consumes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;

@Consumes("application/x.eatable.v2")
public class EatableMessageBodyReaderV2 implements MessageBodyReader<Eatable> {

    private static final MediaType APPLICATION_EATABLE = new MediaType("application", "x.eatable.v2");

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return Eatable.class.isAssignableFrom(type) && mediaType.isCompatible(APPLICATION_EATABLE);
    }

    @Override
    public Eatable readFrom(Class<Eatable> type, Type genericType, Annotation[] annotations, MediaType mediaType,
            MultivaluedMap<String, String> httpHeaders, InputStream entityStream)
            throws IOException, WebApplicationException {
        return EatableMediaTypeV2.parse(entityStream);
    }

}
