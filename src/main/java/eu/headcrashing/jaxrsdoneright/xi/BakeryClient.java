package eu.headcrashing.jaxrsdoneright.xi;

import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;

public class BakeryClient {

    private static final MediaType APPLICATION_EATABLE_V1 = new MediaType("application", "x.eatable.v1");

    private static final MediaType APPLICATION_EATABLE_V2 = new MediaType("application", "x.eatable.v2");

    public static void main(String[] args) {
        Client client = ClientBuilder.newClient();
        try {
            client.register(EatableMessageBodyReaderV1.class);
            client.register(EatableMessageBodyWriterV1.class);
            client.register(EatableMessageBodyReaderV2.class);
            client.register(EatableMessageBodyWriterV2.class);

            URI baseUri = UriBuilder.fromUri("http://localhost/").port(33333).build();
            WebTarget target = client.target(baseUri);
            
            Entity<Apple> entity1 = Entity.entity(new Apple(), APPLICATION_EATABLE_V1);
            Pie pie1 = target.request(APPLICATION_EATABLE_V1).post(entity1, Pie.class);
            System.out.println(pie1);

            Entity<Apple> entity2 = Entity.entity(new Apple(), APPLICATION_EATABLE_V2);
            Pie pie2 = target.request(APPLICATION_EATABLE_V2).post(entity2, Pie.class);
            System.out.println(pie2);
        } finally {
            client.close();
        }
    }

}