package eu.headcrashing.jaxrsdoneright.xi;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;

@Produces("application/x.eatable.v1")
public class EatableMessageBodyWriterV1 implements MessageBodyWriter<Eatable> {

    private static final MediaType APPLICATION_EATABLE = new MediaType("application", "x.eatable.v1");

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return Eatable.class.isAssignableFrom(type) && mediaType.isCompatible(APPLICATION_EATABLE);
    }

    @Override
    public void writeTo(Eatable t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType,
            MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream)
            throws IOException, WebApplicationException {
        EatableMediaTypeV1.render(t, entityStream);
    }

}
