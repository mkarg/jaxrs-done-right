# JAX-RS Done Right

JAX-RS is *not* a substitution for the Servlet API, and it is neither necessary to clutter your code with lots of annotations nor to run it on an application server!

In this mini-series, which is part of [**Head Crashing Informatics**](https://www.youtube.com/channel/UCOPEUog206SxNI-LM6Y8Wwg), I will tell you how to do JAX-RS *the right way*.

Step-by-step you will (re-) learn to use JAX-RS from scratch, the way *it was intended* **by the JAX-RS Specification Authors**.

Since more than a decade I actively and persistently influenced the evolution of the **Java API for RESTful Web Services** aka **Jakarta RESTful Web Services** specification.

I contributed to [JSR 311](https://jcp.org/en/jsr/detail?id=311), defining JAX-RS 1.x contained in Java EE 6, and was an Expert Group Member of [JSR 339](https://jcp.org/en/jsr/detail?id=339) contained in Java EE 7, defining JAX-RS 2.0, and [JSR 370](https://jcp.org/en/jsr/detail?id=370), defining JAX-RS 2.1 contained in Java EE 8.

Since its move to the Eclipse Foundation I am a Committer Memmber of [Jakarta REST](https://projects.eclipse.org/projects/ee4j.jaxrs), defining [the future of JAX-RS](https://github.com/eclipse-ee4j/jaxrs-api).

Chances are good that you already *do* use features and technologies I did invent. Let me show you how I *meant* them to be used, and get more out of our code!


# Volumes

I. - Introduction ([Video](https://youtu.be/h1GjikDN6ww), [Code](I))

II. - Hello, world! (Video, [Code](II))

III. - A client for "Hello, world!" (Video, [Code](III))

IV. - Static Resource Routing

V. - Dynamic Resource Routing

VI. - RegEx Routing

VII. - Parameter Basics

VIII. - Advanced Parameter Topics

IX. - Standard Entity Providers
