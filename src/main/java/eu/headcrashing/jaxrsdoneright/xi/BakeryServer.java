package eu.headcrashing.jaxrsdoneright.xi;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.netty.httpserver.NettyHttpContainerProvider;
import org.glassfish.jersey.server.ResourceConfig;

import io.netty.channel.Channel;

public class BakeryServer {

    public static void main(String[] args) throws InterruptedException {
        URI baseUri = UriBuilder.fromUri("http://localhost/").port(33333).build();
        ResourceConfig resourceConfig = ResourceConfig.forApplication(new BakeryApplication());
        Channel server = NettyHttpContainerProvider.createServer(baseUri, resourceConfig, false);
        System.out.println(server.localAddress());
        Runtime.getRuntime().addShutdownHook(new Thread(server::close));
        Thread.currentThread().join();
    }

}